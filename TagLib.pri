SOURCES += \
	$$PWD/ape/apefile.cpp \
	$$PWD/ape/apefooter.cpp \
	$$PWD/ape/apeitem.cpp \
	$$PWD/ape/apeproperties.cpp \
	$$PWD/ape/apetag.cpp \
	$$PWD/asf/asfattribute.cpp \
	$$PWD/asf/asffile.cpp \
	$$PWD/asf/asfpicture.cpp \
	$$PWD/asf/asfproperties.cpp \
	$$PWD/asf/asftag.cpp \
	$$PWD/audioproperties.cpp \
	$$PWD/fileref.cpp \
	$$PWD/flac/flacfile.cpp \
	$$PWD/flac/flacmetadatablock.cpp \
	$$PWD/flac/flacpicture.cpp \
	$$PWD/flac/flacproperties.cpp \
	$$PWD/flac/flacunknownmetadatablock.cpp \
	$$PWD/it/itfile.cpp \
	$$PWD/it/itproperties.cpp \
	$$PWD/mod/modfile.cpp \
	$$PWD/mod/modfilebase.cpp \
	$$PWD/mod/modproperties.cpp \
	$$PWD/mod/modtag.cpp \
	$$PWD/mp4/mp4atom.cpp \
	$$PWD/mp4/mp4coverart.cpp \
	$$PWD/mp4/mp4file.cpp \
	$$PWD/mp4/mp4item.cpp \
	$$PWD/mp4/mp4properties.cpp \
	$$PWD/mp4/mp4tag.cpp \
	$$PWD/mpc/mpcfile.cpp \
	$$PWD/mpc/mpcproperties.cpp \
	$$PWD/mpeg/id3v1/id3v1genres.cpp \
	$$PWD/mpeg/id3v1/id3v1tag.cpp \
	$$PWD/mpeg/id3v2/frames/attachedpictureframe.cpp \
	$$PWD/mpeg/id3v2/frames/chapterframe.cpp \
	$$PWD/mpeg/id3v2/frames/commentsframe.cpp \
	$$PWD/mpeg/id3v2/frames/eventtimingcodesframe.cpp \
	$$PWD/mpeg/id3v2/frames/generalencapsulatedobjectframe.cpp \
	$$PWD/mpeg/id3v2/frames/ownershipframe.cpp \
	$$PWD/mpeg/id3v2/frames/podcastframe.cpp \
	$$PWD/mpeg/id3v2/frames/popularimeterframe.cpp \
	$$PWD/mpeg/id3v2/frames/privateframe.cpp \
	$$PWD/mpeg/id3v2/frames/relativevolumeframe.cpp \
	$$PWD/mpeg/id3v2/frames/synchronizedlyricsframe.cpp \
	$$PWD/mpeg/id3v2/frames/tableofcontentsframe.cpp \
	$$PWD/mpeg/id3v2/frames/textidentificationframe.cpp \
	$$PWD/mpeg/id3v2/frames/uniquefileidentifierframe.cpp \
	$$PWD/mpeg/id3v2/frames/unknownframe.cpp \
	$$PWD/mpeg/id3v2/frames/unsynchronizedlyricsframe.cpp \
	$$PWD/mpeg/id3v2/frames/urllinkframe.cpp \
	$$PWD/mpeg/id3v2/id3v2extendedheader.cpp \
	$$PWD/mpeg/id3v2/id3v2footer.cpp \
	$$PWD/mpeg/id3v2/id3v2frame.cpp \
	$$PWD/mpeg/id3v2/id3v2framefactory.cpp \
	$$PWD/mpeg/id3v2/id3v2header.cpp \
	$$PWD/mpeg/id3v2/id3v2synchdata.cpp \
	$$PWD/mpeg/id3v2/id3v2tag.cpp \
	$$PWD/mpeg/mpegfile.cpp \
	$$PWD/mpeg/mpegheader.cpp \
	$$PWD/mpeg/mpegproperties.cpp \
	$$PWD/mpeg/xingheader.cpp \
	$$PWD/ogg/flac/oggflacfile.cpp \
	$$PWD/ogg/oggfile.cpp \
	$$PWD/ogg/oggpage.cpp \
	$$PWD/ogg/oggpageheader.cpp \
	$$PWD/ogg/opus/opusfile.cpp \
	$$PWD/ogg/opus/opusproperties.cpp \
	$$PWD/ogg/speex/speexfile.cpp \
	$$PWD/ogg/speex/speexproperties.cpp \
	$$PWD/ogg/vorbis/vorbisfile.cpp \
	$$PWD/ogg/vorbis/vorbisproperties.cpp \
	$$PWD/ogg/xiphcomment.cpp \
	$$PWD/riff/aiff/aifffile.cpp \
	$$PWD/riff/aiff/aiffproperties.cpp \
	$$PWD/riff/rifffile.cpp \
	$$PWD/riff/wav/infotag.cpp \
	$$PWD/riff/wav/wavfile.cpp \
	$$PWD/riff/wav/wavproperties.cpp \
	$$PWD/s3m/s3mfile.cpp \
	$$PWD/s3m/s3mproperties.cpp \
	$$PWD/tag.cpp \
	$$PWD/tagunion.cpp \
	$$PWD/tagutils.cpp \
	$$PWD/toolkit/tbytevector.cpp \
	$$PWD/toolkit/tbytevectorlist.cpp \
	$$PWD/toolkit/tbytevectorstream.cpp \
	$$PWD/toolkit/tdebug.cpp \
	$$PWD/toolkit/tdebuglistener.cpp \
	$$PWD/toolkit/tfile.cpp \
	$$PWD/toolkit/tfilestream.cpp \
	$$PWD/toolkit/tiostream.cpp \
	$$PWD/toolkit/tpropertymap.cpp \
	$$PWD/toolkit/trefcounter.cpp \
	$$PWD/toolkit/tstring.cpp \
	$$PWD/toolkit/tstringlist.cpp \
	$$PWD/toolkit/tzlib.cpp \
	$$PWD/toolkit/unicode.cpp \
	$$PWD/trueaudio/trueaudiofile.cpp \
	$$PWD/trueaudio/trueaudioproperties.cpp \
	$$PWD/wavpack/wavpackfile.cpp \
	$$PWD/wavpack/wavpackproperties.cpp \
	$$PWD/xm/xmfile.cpp \
	$$PWD/xm/xmproperties.cpp

HEADERS += \
	$$PWD/ape/apefile.h \
	$$PWD/ape/apefooter.h \
	$$PWD/ape/apeitem.h \
	$$PWD/ape/apeproperties.h \
	$$PWD/ape/apetag.h \
	$$PWD/asf/asfattribute.h \
	$$PWD/asf/asffile.h \
	$$PWD/asf/asfpicture.h \
	$$PWD/asf/asfproperties.h \
	$$PWD/asf/asftag.h \
	$$PWD/asf/asfutils.h \
	$$PWD/audioproperties.h \
	$$PWD/fileref.h \
	$$PWD/flac/flacfile.h \
	$$PWD/flac/flacmetadatablock.h \
	$$PWD/flac/flacpicture.h \
	$$PWD/flac/flacproperties.h \
	$$PWD/flac/flacunknownmetadatablock.h \
	$$PWD/it/itfile.h \
	$$PWD/it/itproperties.h \
	$$PWD/mod/modfile.h \
	$$PWD/mod/modfilebase.h \
	$$PWD/mod/modfileprivate.h \
	$$PWD/mod/modproperties.h \
	$$PWD/mod/modtag.h \
	$$PWD/mp4/mp4atom.h \
	$$PWD/mp4/mp4coverart.h \
	$$PWD/mp4/mp4file.h \
	$$PWD/mp4/mp4item.h \
	$$PWD/mp4/mp4properties.h \
	$$PWD/mp4/mp4tag.h \
	$$PWD/mpc/mpcfile.h \
	$$PWD/mpc/mpcproperties.h \
	$$PWD/mpeg/id3v1/id3v1genres.h \
	$$PWD/mpeg/id3v1/id3v1tag.h \
	$$PWD/mpeg/id3v2/frames/attachedpictureframe.h \
	$$PWD/mpeg/id3v2/frames/chapterframe.h \
	$$PWD/mpeg/id3v2/frames/commentsframe.h \
	$$PWD/mpeg/id3v2/frames/eventtimingcodesframe.h \
	$$PWD/mpeg/id3v2/frames/generalencapsulatedobjectframe.h \
	$$PWD/mpeg/id3v2/frames/ownershipframe.h \
	$$PWD/mpeg/id3v2/frames/podcastframe.h \
	$$PWD/mpeg/id3v2/frames/popularimeterframe.h \
	$$PWD/mpeg/id3v2/frames/privateframe.h \
	$$PWD/mpeg/id3v2/frames/relativevolumeframe.h \
	$$PWD/mpeg/id3v2/frames/synchronizedlyricsframe.h \
	$$PWD/mpeg/id3v2/frames/tableofcontentsframe.h \
	$$PWD/mpeg/id3v2/frames/textidentificationframe.h \
	$$PWD/mpeg/id3v2/frames/uniquefileidentifierframe.h \
	$$PWD/mpeg/id3v2/frames/unknownframe.h \
	$$PWD/mpeg/id3v2/frames/unsynchronizedlyricsframe.h \
	$$PWD/mpeg/id3v2/frames/urllinkframe.h \
	$$PWD/mpeg/id3v2/id3v2extendedheader.h \
	$$PWD/mpeg/id3v2/id3v2footer.h \
	$$PWD/mpeg/id3v2/id3v2frame.h \
	$$PWD/mpeg/id3v2/id3v2framefactory.h \
	$$PWD/mpeg/id3v2/id3v2header.h \
	$$PWD/mpeg/id3v2/id3v2synchdata.h \
	$$PWD/mpeg/id3v2/id3v2tag.h \
	$$PWD/mpeg/mpegfile.h \
	$$PWD/mpeg/mpegheader.h \
	$$PWD/mpeg/mpegproperties.h \
	$$PWD/mpeg/mpegutils.h \
	$$PWD/mpeg/xingheader.h \
	$$PWD/ogg/flac/oggflacfile.h \
	$$PWD/ogg/oggfile.h \
	$$PWD/ogg/oggpage.h \
	$$PWD/ogg/oggpageheader.h \
	$$PWD/ogg/opus/opusfile.h \
	$$PWD/ogg/opus/opusproperties.h \
	$$PWD/ogg/speex/speexfile.h \
	$$PWD/ogg/speex/speexproperties.h \
	$$PWD/ogg/vorbis/vorbisfile.h \
	$$PWD/ogg/vorbis/vorbisproperties.h \
	$$PWD/ogg/xiphcomment.h \
	$$PWD/riff/aiff/aifffile.h \
	$$PWD/riff/aiff/aiffproperties.h \
	$$PWD/riff/rifffile.h \
	$$PWD/riff/riffutils.h \
	$$PWD/riff/wav/infotag.h \
	$$PWD/riff/wav/wavfile.h \
	$$PWD/riff/wav/wavproperties.h \
	$$PWD/s3m/s3mfile.h \
	$$PWD/s3m/s3mproperties.h \
	$$PWD/tag.h \
	$$PWD/taglib_config.h \
	$$PWD/taglib_export.h \
	$$PWD/tagunion.h \
	$$PWD/tagutils.h \
	$$PWD/toolkit/taglib.h \
	$$PWD/toolkit/tbytevector.h \
	$$PWD/toolkit/tbytevectorlist.h \
	$$PWD/toolkit/tbytevectorstream.h \
	$$PWD/toolkit/tdebug.h \
	$$PWD/toolkit/tdebuglistener.h \
	$$PWD/toolkit/tfile.h \
	$$PWD/toolkit/tfilestream.h \
	$$PWD/toolkit/tiostream.h \
	$$PWD/toolkit/tlist.h \
	$$PWD/toolkit/tmap.h \
	$$PWD/toolkit/tpropertymap.h \
	$$PWD/toolkit/trefcounter.h \
	$$PWD/toolkit/tstring.h \
	$$PWD/toolkit/tstringlist.h \
	$$PWD/toolkit/tutils.h \
	$$PWD/toolkit/tzlib.h \
	$$PWD/toolkit/unicode.h \
	$$PWD/trueaudio/trueaudiofile.h \
	$$PWD/trueaudio/trueaudioproperties.h \
	$$PWD/wavpack/wavpackfile.h \
	$$PWD/wavpack/wavpackproperties.h \
	$$PWD/xm/xmfile.h \
	$$PWD/xm/xmproperties.h

INCLUDEPATH += $$PWD/toolkit
INCLUDEPATH += $$PWD/asf
INCLUDEPATH += $$PWD/mpeg
INCLUDEPATH += $$PWD/ogg
INCLUDEPATH += $$PWD/ogg/flac
INCLUDEPATH += $$PWD/flac
INCLUDEPATH += $$PWD/mpc
INCLUDEPATH += $$PWD/mp4
INCLUDEPATH += $$PWD/ogg/vorbis
INCLUDEPATH += $$PWD/ogg/speex
INCLUDEPATH += $$PWD/ogg/opus
INCLUDEPATH += $$PWD/mpeg/id3v2
INCLUDEPATH += $$PWD/mpeg/id3v2/frames
INCLUDEPATH += $$PWD/mpeg/id3v1
INCLUDEPATH += $$PWD/ape
INCLUDEPATH += $$PWD/wavpack
INCLUDEPATH += $$PWD/trueaudio
INCLUDEPATH += $$PWD/riff
INCLUDEPATH += $$PWD/riff/aiff
INCLUDEPATH += $$PWD/riff/wav
INCLUDEPATH += $$PWD/mod
INCLUDEPATH += $$PWD/s3m
INCLUDEPATH += $$PWD/it
INCLUDEPATH += $$PWD/xm
INCLUDEPATH += $$PWD
